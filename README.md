# Report Creator for Vivify Scrum

## To Run

1. Clone the source code from [bitbucket](https://bitbucket.org/everything-is-temporary/origin-trail-sprint-report-creator/overview) (`git clone https://bitbucket.org/everything-is-temporary/origin-trail-sprint-report-creator.git`)
2. Install dependencies from `requirements.txt`
3. Download json data files from Vivifyscrum
    1. From the sprint view, click on the "more menu" (3 vertical dots)
    2. Click "Export Sprint"
    3. Change the first option to "Sprint items"
    4. Change the second option to "JSON file"
    5. Click "Export"
2. Use the following command:
    - `report_creator.py JSON_DATA_FILES [-o OUTPUT_FILENAME]`
    - Arguments in brackets are optional.

## Documentation

[Dominate](https://github.com/Knio/dominate#dominate)