import argparse
from vivify_data_container import VivifyDataStore
import os
import warnings
import datetime

import dominate
from dominate.tags import *


class ReportCreator:

    def __init__(self, vds_obj):
        self.vds = vds_obj

    @staticmethod
    def _hpair(header_name, element):
        l = li(cls='subitem')
        l.add(h4(header_name, cls='subitemheader'))
        l.add(element)
        return l

    def _write_sprint_item(self, sprint_item):
        l = ul()
        if sprint_item.points_difficulty and sprint_item.points_value:
            points_element = ul(cls="points")
            points_element.add(li("Difficulty: {}".format(sprint_item.points_difficulty)))
            points_element.add(li("Value: {}".format(sprint_item.points_value if sprint_item.points_value > 1 else 'Pending Further Discussion')))
            l.add(self._hpair("Point Values", points_element))

        if len(sprint_item.estimates):
            estimates_element = ul(cls="estimates")
            for estimate_key, estimate_value in sprint_item.estimates.items():
                estimates_element.add(li("{0}: {1}".format(estimate_key, estimate_value)))
            l.add(self._hpair("Hour Estimates", estimates_element))

        if len(sprint_item.checklists) > 0:
            all_checklist_items = []
            for checklist in sprint_item.checklists.values():
                all_checklist_items += checklist
            checklists_element = ol(cls="checklists")
            for criteria in all_checklist_items:
                checklists_element.add(li(criteria))
            l.add(self._hpair("Acceptance Criteria", checklists_element))

        if len(sprint_item.assignees) > 0:
            assigned_users_element = ul(cls="assignees")
            for name in sprint_item.assignees:
                assigned_users_element.add(li(name))
            l.add(self._hpair("Assigned to:", assigned_users_element))

        if len(sprint_item.children) > 0:
            subtask_list = ol()
            for child in sprint_item.children:
                subtask_list.add(self._write_sprint_item(child))
            l.add(self._hpair("Subtasks", subtask_list))
        return self._hpair(h3(sprint_item.story), l)

    def create_sprint_category(self, sprint_category, sprint_items):
        d = div(cls="category")
        d.add(h2(sprint_category, cls="category_header"))
        d.add(h3("User Stories"))
        element_sprint_items = ol()
        for sprint_item in sprint_items:
            element_sprint_items.add(self._write_sprint_item(sprint_item))
        d.add(element_sprint_items)
        return d

    def create_report(self, output_filename):
        doc = dominate.document(title="Origin Trail {}".format(self.vds.sprint_name))
        with doc.head:
            meta(charset='utf-8')  # https://github.com/Knio/dominate/issues/62
        doc.add(h1(self.vds.sprint_name))
        for category_items in self.vds.sprint_items.items():
            doc.add(hr())
            doc.add(self.create_sprint_category(*category_items))
        with open(output_filename, 'w') as f:
            f.write(doc.render())


def _add_data_file_to_vds(vds, file_or_dir_name):
    if os.path.isdir(file_or_dir_name):
        for file in sorted(os.listdir(file_or_dir_name)):
            _add_data_file_to_vds(vds, os.path.join(file_or_dir_name, file))
        return
    if file_or_dir_name.endswith('.json'):
        vds.import_data_file(file_or_dir_name)
    else:
        warnings.warn("Cannot import data from {0} (is not a json file).".format(file_or_dir_name))


def main(parsed_args):
    vds = VivifyDataStore()
    for file_or_dir_name in parsed_args.data_files:
        _add_data_file_to_vds(vds, file_or_dir_name)

    rc = ReportCreator(vds)

    output_filename = parsed_args.output_file if parsed_args.output_file is not None else "{0}-report.html".format(
        datetime.datetime.now().isoformat())

    rc.create_report(output_filename)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create Sprint Reports Automatically with the JSON exports from VivifyScrum')
    parser.add_argument('data_files', nargs='+', help='Any number of json files, or directories containing json files, to use as the data for this program.')
    parser.add_argument('-o', dest='output_file', help='The filename of the generated report.', default=None)

    main(parser.parse_args())


