import re
import json
from collections import OrderedDict

# JSON keys

jkey_category_name = "name"
jkey_status_divided_items = "items"
jkey_item_id = "id"
jkey_item_name = "name"
jkey_item_user_story_main = "description"
jkey_item_source_board = "board"
jkey_item_value_points = "value"
jkey_item_difficulty_points = "points"
jkey_item_acceptance_criteria = "checklists"
jkey_item_acceptance_criteria_items = "items"
jkey_item_acceptance_criteria_item_text = "description"
jkey_item_tags = "labels"
jkey_item_tags_text = "name"
jkey_item_tags_color = "color"
jkey_item_parent = "parent"
jkey_item_assigned_user_list = "assignees"
jkey_item_assigned_user_name = "name"

ignore_item_ids = [1423981]

# Non-JSON keys

key_item_title = "ItemTitle"
key_optimistic_estimate = "Optimistic"
key_expected_estimate = "Expected"
key_pessimistic_estimate = "Pessimistic"


estimate_regex_string = r'^(?P<{0}>.*)\s\([Oo].*\:\s*(?P<{1}>\d+).*[Ee].*\:\s*(?P<{2}>\d+).*[Pp].*\:\s*(?P<{3}>\d+).*\)$'.format(key_item_title, key_optimistic_estimate, key_expected_estimate, key_pessimistic_estimate)
estimate_regex = re.compile(estimate_regex_string)


class SimpleVivifyItemContainer:
    def __init__(self, json_item):
        self.id = json_item[jkey_item_id]
        self.story = json_item[jkey_item_user_story_main]
        self.board = json_item[jkey_item_source_board]
        self.checklists = {index: list(
            item[jkey_item_acceptance_criteria_item_text] for item in checklist[jkey_item_acceptance_criteria_items])
                           for index, checklist in enumerate(json_item[jkey_item_acceptance_criteria])}
        self.points_difficulty = int(json_item[jkey_item_difficulty_points])
        self.points_value = json_item[jkey_item_value_points]
        self.item_tags = {tag[jkey_item_tags_text]: {jkey_item_tags_color: tag[jkey_item_tags_color]} for tag in
                          json_item[jkey_item_tags]}
        self.estimates = OrderedDict()
        match = estimate_regex.fullmatch(json_item[jkey_item_name])
        if match:
            for key, val in match.groupdict().items():
                if key == key_item_title:
                    self.item_name = val
                else:
                    self.estimates[key] = val
        else:
            self.item_name = json_item[jkey_item_name]
        self.parent_id = json_item[jkey_item_parent][jkey_item_id] if json_item[jkey_item_parent] else None
        self.children = []
        self.assignees = list(user[jkey_item_assigned_user_name] for user in json_item[jkey_item_assigned_user_list])

    def add_child(self, child):
        self.children.append(child)

    def __repr__(self):
        return self.item_name


class VivifyDataStore:

    def __init__(self):
        self.sprint_items = OrderedDict()  # sorted by board
        self.sprint_name = None

    def _import_data_json(self, json_data): # assumes each individual import contains parents of its children
        items = {}
        for category in json_data:
            for item in category[jkey_status_divided_items]:
                items[item[jkey_item_id]] = SimpleVivifyItemContainer(item)
        for svic in items.values():
            if svic.id in ignore_item_ids:
                continue
            if svic.board not in self.sprint_items:
                self.sprint_items[svic.board] = []
            if svic.parent_id is not None:
                if svic.parent_id not in items:
                    raise ValueError("Parent of child not found in data.")
                items[svic.parent_id].add_child(svic)
            else:
                self.sprint_items[svic.board].append(svic)
        try:
            self.sprint_name = json_data[0][jkey_status_divided_items][0]["sprint"]
        except KeyError:
            pass

    def import_data_file(self, *filepaths):
        for filepath in filepaths:
            with open(filepath) as f:
                self._import_data_json(json.load(f))
